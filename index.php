<?php get_header(); ?>

<header id="site-header">
	<div class="container">
		<a href="<?php echo get_site_url('/') ?>">← <?php _e('Back') ?></a>
		<h1 id="index-title"><?php is_home() && !is_front_page() ? single_post_title() : the_archive_title() ?></h1>
		<h2 id="index-description"><?php the_archive_description() ?></h2>
		<?php if (is_search()) : ?>
			<?php get_search_form() ?>
		<?php endif ?>
	</div>
</header>

<main id="site-content">
	<div class="container">
		<div id="post-list">
			<?php if (have_posts()) : ?>

				<?php while (have_posts()) : the_post(); ?>
					<article id="post-<?php echo the_ID() ?>" class="post">
						<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
						<p><?php the_content() ?></p>
					</article>
				<?php endwhile; ?>

				<?php
					the_posts_pagination([
						'mid_size' => 5,
						'prev_text' => '&lt;',
						'next_text' => '&gt;',
					]);
				?>
			<?php else : ?>
				<p><?php _e('Here is nothing') ?></p>
			<?php endif; ?>

		</div><!-- #post-list -->
	</div><!-- /.container -->
</main><!-- /.site-content -->

<?php get_footer();
