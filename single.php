<?php get_header(); ?>

<?php if (have_posts()) : the_post(); ?>

<header id="site-header">
	<div class="container">
		<a href="<?php echo guoyunhe_get_post_back_link(get_the_category()) ?>">← <?php _e('Back') ?></a>
	</div>
</header>

<main id="site-content">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" class="single-post">

			<h1 class="post-title"><?php the_title() ?></h1>

			<?php the_content() ?>

			<?php
				the_post_navigation([
					'prev_text' => '<span class="nav-title">&lt; %title</span>',
					'next_text' => '<span class="nav-title">%title &gt;</span>',
				]);

				if (comments_open() || get_comments_number()) :
					comments_template();
				endif;
			?>
		</article><!-- #post -->

	</div><!-- /.container -->
</main><!-- /#site-content -->

<?php endif; ?>

<?php get_footer();
