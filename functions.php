<?php

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function guoyunhe_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/guoyunhe
     * If you're building a theme based on Guo Yunhe 2, use a find and replace
     * to change 'guoyunhe' to the name of your theme in all the template files.
     */
    load_theme_textdomain('guoyunhe');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus([
        'content' => __('Content', 'guoyunhe')
     ]);

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', [
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
     ]);

    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support('post-formats', [
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
     ]);

    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', [
        'width'       => 250,
        'height'      => 250,
        'flex-width'  => true,
     ]);

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, and column width.
      */
    add_editor_style([ get_stylesheet_uri() , get_theme_file_uri('/css/editor-style.css') ]);

    // Define and register starter content to showcase the theme on new sites.
    $starter_content = [
        'posts' => [
            'home',
            'about' => [
                'thumbnail' => '{{image-sandwich}}',
            ],
            'contact' => [
                'thumbnail' => '{{image-espresso}}',
            ],
            'blog' => [
                'thumbnail' => '{{image-coffee}}',
            ],
            'homepage-section' => [
                'thumbnail' => '{{image-espresso}}',
            ],
        ],
        'attachments' => [
            'image-espresso' => [
                'post_title' => _x('Espresso', 'Theme starter content', 'guoyunhe'),
                'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
            ],
            'image-sandwich' => [
                'post_title' => _x('Sandwich', 'Theme starter content', 'guoyunhe'),
                'file' => 'assets/images/sandwich.jpg',
            ],
            'image-coffee' => [
                'post_title' => _x('Coffee', 'Theme starter content', 'guoyunhe'),
                'file' => 'assets/images/coffee.jpg',
            ],
        ],

        // Default to a static front page and assign the front and posts pages.
        'options' => [
            'show_on_front' => 'page',
            'page_on_front' => '{{home}}',
            'page_for_posts' => '{{blog}}',
        ],

        // Set up nav menus for each of the two areas registered in the theme.
        'nav_menus' => [
            // Assign a menu to the "top" location.
            'content' => [
                'name' => __('Top navigation menu', 'guoyunhe'),
                'items' => [
                    'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
                    'page_about',
                    'page_blog',
                    'page_contact',
                ],
            ],
        ],
    ];

    $starter_content = apply_filters('guoyunhe_starter_content', $starter_content);

    add_theme_support('starter-content', $starter_content);
}
add_action('after_setup_theme', 'guoyunhe_setup');

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function guoyunhe_pingback_header()
{
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s">' . "\n", get_bloginfo('pingback_url'));
    }
}
add_action('wp_head', 'guoyunhe_pingback_header');

/**
 * Enqueue scripts and styles.
 */
function guoyunhe_scripts()
{
    wp_enqueue_style('normalize.css', get_theme_file_uri('/css/normalize.css', '8.0.0'));
    wp_enqueue_style('animate.css', get_theme_file_uri('/css/animate.css', '3.7.0'));
    wp_enqueue_style('guoyunhe-style', get_stylesheet_uri(), '20181010');

    wp_enqueue_script('fastclick', get_theme_file_uri('/js/fastclick.js'), [], '1.0.6', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'guoyunhe_scripts');

function guoyunhe_filter_chinese_excerpt($output)
{
    global $post;

    $chinese_output = preg_match_all("/\p{Han}+/u", $post->post_content, $matches);

    if ($chinese_output) {
        $output = mb_substr($output, 0, 100) . '...';
    }

    return $output;
}
add_filter('get_the_excerpt', 'guoyunhe_filter_chinese_excerpt');

function guoyunhe_get_post_back_link($cats)
{
    // Go to reference URL if it is an archive page
    if (isset($_SERVER['HTTP_REFERER'])) {
        $refer = $_SERVER['HTTP_REFERER'];
        $request = $_SERVER['REQUEST_URI'];
        $refer_data = parse_url($refer);
        $request_data = parse_url($request);

        if (isset($_SERVER['HTTP_HOST']) && $refer_data['host'] === $_SERVER['HTTP_HOST']) {
            // Home/search, blog, categories, tags, date archives
            if ($refer_data['path'] === '/'
                || strpos($refer_data['path'], 'blog')
                || strpos($refer_data['path'], 'category')
                || strpos($refer_data['path'], 'tag')
                || preg_match('/^\/\d{4}\/(\d{2}\/)?(\d{2}\/)?(page\/\d+\/)?$/', $refer_data['path'])) {
                return $refer;
            }
        }
    }

    // Otherwise, go to a category page
    if ($cats) {
        return get_category_link($cats[0]->term_id);
    }

    // Fallback, go to home page
    return home_url();
}
