<?php get_header(); ?>

<header id="site-header">
	<div class="container">
		<h1 id="site-name"><?php bloginfo('name'); ?></h1>
		<p id="site-description"><?php bloginfo('description'); ?></p>
	</div><!-- /.container -->
</header><!-- /#site-header -->

<main id="site-content">
	<div class="container">
		<?php
			wp_nav_menu([
				'theme_location' => 'content',
				'menu_id' => 'site-content-menu',
				'container' => 'ul',
			]);
		?>
	</div><!-- /.container -->
</main><!-- /#site-content -->

<?php get_footer();
