<footer id="site-footer" role="contentinfo">
	<div class="container">
		<hr />
		<p>
            &copy; 2011&ndash;<?php echo date("Y"); ?> 郭云鹤(Guo Yunhe)
            |
            <a href="/">中文</a>
            |
            <a href="/en">English</a>
        </p>
	</div><!-- .container -->
</footer><!-- #site-footer -->

<?php wp_footer(); ?>

</body>
</html>
