<?php get_header(); ?>

<?php if (have_posts()) : the_post(); ?>

<header id="site-header">
	<div class="container">
		<a href="<?php echo guoyunhe_get_post_back_link(get_the_category()) ?>">← <?php _e('Back') ?></a>
	</div>
</header>

<main id="site-content">
	<div class="container">

		<article id="page-<?php the_ID(); ?>" class="single-post">

			<h1 class="post-title"><?php the_title() ?></h1>

			<?php the_content() ?>

			<?php
				if (comments_open() || get_comments_number()) :
					comments_template();
				endif;
			?>
		</article><!-- #page -->

	</div><!-- /.container -->
</main><!-- /#site-content -->

<?php endif; ?>

<?php get_footer();
